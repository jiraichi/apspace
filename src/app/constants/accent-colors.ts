export const accentColors = [
  { title: 'Sky (Default)', name: 'blue', value: '#3a99d9', rgba: '58, 153, 217' },
  { title: 'Forest', name: 'green', value: '#08a14f', rgba: '8, 161, 79' },
  { title: 'Fire', name: 'red', value: '#e54d42', rgba: '229, 77, 66' },
  { title: 'Flower', name: 'pink', value: '#ec2a4d', rgba: '236, 42, 77' },
  { title: 'Lightning', name: 'yellow', value: '#dfa847', rgba: '223, 168, 71' },
  { title: 'White', name: 'white', value: '#b0acac', rgba: '175, 175, 175' }, // only for pure dark
];
