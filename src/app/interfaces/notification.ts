export interface NotificationStatus {
    active: boolean;
}

export interface NotificationSubStatus {
    msg: string;
}
