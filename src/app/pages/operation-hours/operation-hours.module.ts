import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { IonicModule } from '@ionic/angular';

import { ComponentsModule } from 'src/app/components/components.module';
import { SharedPipesModule } from '../../shared/shared-pipes.module';
import { FilterByCompanyPipe } from './filter-by-company.pipe';
import { FormattedDatePipe } from './formatted-date.pipe';
import { OperationHoursPage } from './operation-hours.page';
import { TimePipe } from './time.pipe';

const routes: Routes = [
  {
    path: '',
    component: OperationHoursPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    SharedPipesModule
  ],
  declarations: [OperationHoursPage, TimePipe, FilterByCompanyPipe, FormattedDatePipe]
})
export class OperationHoursPageModule { }
