export interface AttendanceDetails {
    ATTENDANCE_STATUS: string;
    CLASS_DATE: string;
    CLASS_TYPE: string;
    TIME_FROM: string;
    TIME_TO: string;
}
